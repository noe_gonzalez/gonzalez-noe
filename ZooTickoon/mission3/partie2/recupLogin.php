<!DOCTYPE html> <!-- Gonzalez Noé -->
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Espace de connexion</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
<link rel="stylesheet" href="css/bootstrap.min.css">
<script src="code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</head>
    <body>
        <div style="text-align: left">
            <h1>Mon espace client</h1>
            <hr width="15.5%" size="3%" align="left" color="black">
            <?php
            $user_login = $_GET['email'];
            $user_pass = $_GET['mdp'];
            $utilisateurs = array('noe48713@gmail.com', 'noe','Lina@gmail.com', 'passeLina', 'Edgar@gmail.com', 'passeEdgar');
            if ( in_array($user_login,$utilisateurs) && (in_array($user_pass ,$utilisateurs))) {
                    
            ?>

            <p>Vous êtes connecté à <strong><?php echo $user_login?></strong></p>
            <p>Bienvenue sur votre espace client.
            </p>

            <?php
            } else {
            ?>

            <p><span class="error">Une Erreur et survenu lors de la connexion:</span> <a href="formLogin.html">Cliquez</a> ici pour vous identifier à nouveau.
            <br>
                Si vous avez perdu vos identifiants, veuillez contactez le service clients.
            <br>
            <br>
            
            <label for="emails">Voici la liste des emails disponibles actuellement</label>

            <select name="emails" id="list">
                <option value="Lina@gmail.com">Lina@gmail.com</option>
                <option value="Edgar@gmail.com">Edgar@gmail.com</option>
                <option value="noe48713@gmail.com">noe48713@gmail.com</option>
            </select>
            <?php
                }
            ?>
        </div>
    </body>
</html>