#include <stdio.h>

int main() {
    int Tab[26] = {0},i = 0,letters = 0;
    char phrase[10000];
    scanf("%[^\n]",phrase);
    while (phrase[i] != 0){
        if (phrase[i] >= 97 && phrase[i] <= 122){
            Tab[phrase[i]-97] += 1;
            letters++;
        } else if (phrase[i] >= 65 && phrase[i] <= 90){
            Tab[phrase[i]-65] += 1;
            letters++;
        }
        i++;
    }
    int max = 0;
    for (int index = 0;index < 26;index++){
        if ((double)(Tab[max])/letters < (double)(Tab[index])/letters){
            max = index;
        }   
    }
    int decalage = max+65 - 69;
    int v = 0;
    int temp;
    while (phrase[v] != 0) {
        if (97 <= phrase[v] && phrase[v] <= 122){
            temp = (phrase[v] - decalage - 97);
            phrase[v] = (((temp % 26 )+ 26 )%26)+97;
            
        } else if (65 <= phrase[v] && phrase[v] <= 90){
            temp = (phrase[v] - decalage - 65);
            phrase[v] = (((temp % 26 )+ 26 )%26)+65;
        }
        v++;
    }
    printf("%s",phrase);
         
}
