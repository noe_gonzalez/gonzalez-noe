#include <stdio.h>

int split_nbr(int);
int convert(char *);

int main(void)
{
    char name_1[51];
    char name_2[51];
    int count_1;
    int count_2;

    scanf("%s %s", name_1, name_2);

    count_1 = convert(name_1);
    count_2 = convert(name_2);
    if (count_1 < 10)
        printf("%d ", count_1);
    else
    {
        while (!(count_1 < 10))
            count_1 = split_nbr(count_1);
        printf("%d ", count_1);
    }
    if (count_2 < 10)
        printf("%d\n", count_2);
    else
    {
        while (!(count_2 < 10))
            count_2 = split_nbr(count_2);
        printf("%d\n", count_2);
    }
    return (0);
}

int convert(char *str)
{
    char alpf[27] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    int arr[26] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25};
    int count;
    int i;
    int j;

    count = 0;
    i = 0;
    while (str[i])
    {
        j = 0;
        while (alpf[j] && j < (sizeof(arr) / sizeof(arr[0])))
        {
            if (str[i] == alpf[j])
                count += arr[j];
            j++;
        }
        i++;
    }
    return (count);
}

int split_nbr(int nbr)
{
    int mod;
    int res;

    mod = 0;
    res = 0;

    while (nbr > 0)
    {
        mod = nbr % 10;
        res += mod;
        nbr /= 10;
    }
    return (res);
}
