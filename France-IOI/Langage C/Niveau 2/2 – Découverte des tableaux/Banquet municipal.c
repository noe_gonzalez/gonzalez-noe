#include <stdio.h>

int main() {
    int nb, nbchangement,positiondep,position1,position2,intermediaire;
    scanf("%d %d",&nb,&nbchangement);
    int tab[nb];
    for(int index = 0; index < nb;index++){
        scanf("%d",&positiondep);
        tab[index] = positiondep;
    }
    for(int index = 0; index < nbchangement;index++){
        scanf("%d %d",&position1,&position2);
        intermediaire = tab[position1];
        tab[position1] = tab[position2];
        tab[position2] = intermediaire;
    }
    for(int index = 0; index < nb; index++){
        printf("%d\n",tab[index]);
    }
}
