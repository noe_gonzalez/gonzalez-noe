#include <stdio.h>

int main() {
    int nb;
    scanf("%d",&nb);
    int tab[nb],argent;
    for (int loop = 0;loop < nb;loop++){
        scanf("%d",&argent);
        tab[loop] = argent;
    }

    int x,j;
    for (int index = 0;index < nb;index++){
        x = tab[index];
        j = index;
        while (j > 0 && tab[j-1] > x) {
            tab[j] = tab[j-1];
            j--;
        }
        tab[j] = x;
    }

    if (nb % 2 == 0){
        printf("%lf",(tab[nb/2]+tab[(nb/2)-1])/2.0);
    } else {
        printf("%d", (tab[(nb/2)]));
    }
}
