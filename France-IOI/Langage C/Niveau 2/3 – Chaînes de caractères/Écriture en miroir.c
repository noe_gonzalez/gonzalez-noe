#include <stdio.h>

int main() {
    int nb,count = 0,end,begin;
    scanf("%d\n",&nb);
    char text[1000],textReversed[1000];
    for (int index = 0;index < nb;index++){
        scanf("%[^\n]\n",text);
        count = 0;
        while (text[count] != 0)
            count++;

        end = count - 1;
        for (begin = 0; begin < count; begin++) {
            textReversed[begin] = text[end];
            end--;
        }

        textReversed[begin] = 0;
        printf("%s\n", textReversed);
    }
}
